# Exemplo de formatação Markdown

## Cabeçalho de nível 2
E aqui mais um bloco de texto..

e bla, bla, bla

### Cabeçalho de nivel 3

E assim vai... vai...
Até o nível 6

###### Cabeçalho de nível 6

e é isso


## Formatação - negrito e itálico

Para dar ênfase usar asteriscos:

Um texto com **ênfase** em uma palavra (2 asteriscos - negrito)

Uma linha com *ênfase* em uma palavra (1 asteriscos - itálico)


## Citações

Podemos fazer um bloco de citação de texto. Ele coloca em destaque um texto que estamos citando de outra fonte - texto que não foi escrito por nós - ou que queremos destacar simplesmente. 

Assim:

> Isto agora é uma citação. Repare que o texto fica destacado do restante do documento.
>
> E a citação pode ter parárafos também.
>
>> E também pode ter vários níveis
>
> Assim fica uma citação dentro de outra


## Listas 

Podemos criar listas usando - e . 

## Uma lista não ordenada

- Um ítem da lista
- Outro ítem
- mais um
- e assim vai...

## Uma lista ordenada

1. Esta lista tem uma ordem
1. cada ítem
1. tem um 
1. número 
1. indicando
1. a ordem que 
1. ele ocupa na lista


## Imagens



![Tux, mascote do Linux](img/tux.avif)

![Um pato](img/pato.jpg)


## Blocos de código


Este é um exemplo de código python que foi simplesmente copiado e colado aqui:

#!/usr/bin/python
experiencia = 20
pts_experiencia = 0.05
pts_magia = 0.5
robos = 2

resultado = (robos * pts_experiencia) + (robos * pts_magia) + (robos * experiencia)

print(f'Você ganhou {resultado} pontos!')


Bem ruím... Isso é porque a fonte usada para mostrar esse bloco de texto não é *monoespaçada* - os caracteres não ocupam todos o mesmo espaço na linha.

Precisamos usar uma fonte **monoespaçada* para representar corretamente um trecho de código.


``` python
#!/usr/bin/python
experiencia = 20
pts_experiencia = 0.05
pts_magia = 0.5
robos = 2

resultado = (robos * pts_experiencia) + (robos * pts_magia) + (robos * experiencia)

print(f'Você ganhou {resultado} pontos!')
```

## Links

- Link para um site na Internet (URL)

Minha ferramenta de busca preferida é o [Duck Duck Go](https://duckduckgo.com).

- Link para um outro documento (página) 

Ex: Clique para ir para a [próxima página](uma_pagina.md).
